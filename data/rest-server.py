#!venv/bin/python

"""Data RESTful server implemented."""

from flask import Flask, jsonify, abort, make_response
from flask.ext.restful import Api, Resource, reqparse, fields, marshal
from flask.ext.httpauth import HTTPBasicAuth

app = Flask(__name__, static_url_path="")
api = Api(app)
auth = HTTPBasicAuth()

@auth.get_password
def get_password(username):
    if username == 'briner':
        return 'python'
    return None

@auth.error_handler
def unauthorized():
    # return 403 instead of 401 to prevent browsers from displaying the default
    # auth dialog
    return make_response(jsonify({'message': 'Unauthorized access'}), 403)


tms = [
    {
        "id": 'fe1c448d-f2e8-4fe4-a6ec-cf9785dc46c9',
        "user_id": 'briner',
        'email': 'briner@infomaniak.ch',
    },
    {
        "id": '7b31a9f7-a58b-4829-891a-eaed70702943',
        "user_id": 'briner',
        'email': 'briner@infomaniak.ch',
    },
    {
        "id": '7b31a9f7-a58b-4829-891a-eaed70702943',
        "user_id": 'gattusox',
        'email': 'cedric.briner@unige.ch',
    }
]

tm_fields = {
    'id': fields.String,
    'user_id': fields.String,
    'email': fields.Boolean,
    'uri': fields.Url('task')
}